/* Example code demonstrating the use of pointers.
 * It uses the hardware UART on the MSP430G2553 to receive
 * and transmit data back to a host computer over the USB connection on the MSP430
 * launchpad.
 * Note: After programming using CCS it is necessary to stop debugging and reset the uC before
 * connecting the terminal program to transmit and receive uint8_tacters.
 * This demo will transmit the uint8_tacters AB in response to a S been sent each time S is sent the
 * order of the uint8_tacters is swaped. If agen any other uint8_tacter is sent and unknown command response is sent.
 */

#include "msp430g2553.h"
#include <stdint.h>
#include <string.h>

void UARTSendArray(uint8_t *TxArray, uint8_t ArrayLength);
void UARTSendChar(volatile uint8_t *Txuint8_t);
void SwapChars(uint8_t *a, uint8_t *b);
void uart_config();

volatile uint8_t Data;

void main(void)
{
	uint8_t bufferghini[20];
	uint8_t * p_buff;
	uint8_t x1 = '0';
	uint8_t x2 = '2';
	
	WDTCTL = WDTPW + WDTHOLD; // Stop WDT
	P1DIR |= BIT0;    // Set the LEDs on P1.0, P1.6 as outputs
	// P2DIR |= BIT0;
	P1OUT |= BIT0;            // Set P1.0
	P1OUT &= ~BIT0;
	
	BCSCTL1 = CALBC1_1MHZ;   // Set DCO to 1MHz
	DCOCTL = CALDCO_1MHZ;    // Set DCO to 1MHz
	
	/* Configure hardware UART */
	uart_config();
	UARTSendArray("HOla mundo\r\n", strlen("HOla mundo\r\n"));
	
	while(1)
	{
		__bis_SR_register(LPM0_bits + GIE); // Enter LPM0, interrupts enabled
		/* (uint8_t *)bufferghini = Data; */
		switch(Data)
		{
			case 'S':
			{
				SwapChars(&x1,&x2);     // Pass the pointers to variables x1 and x2 to the function
				UARTSendChar(&x1);
				UARTSendChar(&x2);
				UARTSendArray("\r\n", 2);
				P1OUT &= ~BIT4;            // Set P1.0
				}
				break;
			default:
			{
					UARTSendArray("Unknown Command: ", 17);
					UARTSendChar( &Data );
					UARTSendArray("\r\n", 2);
			}
			break;
		}
        P1OUT ^= BIT0;
		
	}
}


#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void)
{
  Data = UCA0RXBUF;
  P1OUT ^= BIT0;
  __bic_SR_register_on_exit(LPM0_bits);
}

void UARTSendArray(uint8_t *TxArray, uint8_t ArrayLength)
{
	/* Send number of bytes Specified in ArrayLength in the array at using the hardware UART 0
	Example usage: UARTSendArray("Hello", 5);
	int data[2]={1023, 235};
	UARTSendArray(data, 4);  // Note because the UART transmits bytes it is necessary to send two bytes for each integer hence the data length is twice the array length */

	while(ArrayLength--)
	{             // Loop until StringLength ==s 0 and post decrement
		while(!(IFG2 & UCA0TXIFG));      // Wait for TX buffer to be ready for new data
		UCA0TXBUF = *TxArray++;           //Write the uint8_tacter at the location specified by the pointer and post increment
	}
}

void UARTSendChar(volatile uint8_t *Txuint8_t)
{
	  // Send number of bytes Specified in ArrayLength in the array at using the hardware UART 0
	  // Example usage: UARTSendArray('A');
	  while(!(IFG2 & UCA0TXIFG));      // Wait for TX buffer to be ready for new data
	  UCA0TXBUF = *Txuint8_t;           //Write the uint8_tacter at the location specified py the pointer
}


void uart_config()
{
	P1SEL = BIT1 + BIT2 ;    // P1.1 = RXD, P1.2=TXD
	P1SEL2 = BIT1 + BIT2 ;   // P1.1 = RXD, P1.2=TXD
	UCA0CTL1 |= UCSSEL_2;    // Use SMCLK
	UCA0BR0 = 104;           // Set baud rate to 9600 with 1MHz clock (Data Sheet 15.3.13)
	UCA0BR1 = 0;             // Set baud rate to 9600 with 1MHz clock
	UCA0MCTL = UCBRS0;       // Modulation UCBRSx = 1
	UCA0CTL1 &= ~UCSWRST;    // Initialize USCI state machine
	IE2 |= UCA0RXIE;         // Enable USCI_A0 RX interrupt
}
void SwapChars(uint8_t *a, uint8_t *b)
{
	uint8_t c = 0;      // initialise temporary variable c
	P1OUT |= BIT6;            // Set P1.0
	c = *a;      // copy the value in memory location a in variable c
	*a = *b;     // copy the value stored in memory location b into memory location a
	*b = c;      // copy the value temporarily stored in c into memory location b
}
